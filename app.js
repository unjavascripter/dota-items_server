'use strict';

const express = require('express');
const app = express();

const seed = require('./seed.js');

// CORS
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods',' GET,PUT,POST,DELETE');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  next();
});


// Auth
let isAuthenticated = (req, res, next) => {
  let allowedTokens = ['Bearer 800813555'];
  let authToken = req.headers['authorization'];

  if(!authToken){
    return res.status(403).send({ message: 'No Authorization header present' }).end();
  }

  if(allowedTokens.indexOf(authToken) === -1) {
    return res.status(401).send({ message: 'Invalid token' }).end();
  }

  next();

};


// API Responses
let respond = (res, status, data) => {
  if(!data){
    return res.status(status).end();
  }
  return res.status(status).send(data).end();
}



// API

app.get('/api/items', (req, res) => {
  return respond(res, 200, seed);
});

// app.get('/api/item/:id', (req, res) => {
//     return respond(res, 200, ****);
// });



app.listen(3000, _ => console.log('API running on port 3000'));